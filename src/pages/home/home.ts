import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SentencePage } from './../sentence/sentence';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  sentFunc(tSent) {
  	tSent=tSent;
    this.navCtrl.push(SentencePage,{
    	option:tSent
    });
  }

}
