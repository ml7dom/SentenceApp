import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {HomePage} from "../home/home";

/**
 * Generated class for the SentencePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sentence',
  templateUrl: 'sentence.html',
})
export class SentencePage {

	tSent: string;
	tiSent: string;
	stiSent: string;

  verbs:any[];
  pronouns:any[];
  complements:any[];
  etime:any[];

  s_verb:any[];

  sAfirm:string;
  sNegat:string;
  sInte:string;
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.tSent=navParams.get('option');
    this.goLoad();
  }

  ionViewDidLoad() {
    
  }

  goLoad(){
    this.pronouns=[
      {id:1, name:'i'},
      {id:2, name:'you'},
      {id:3, name:'he'},
      {id:4, name:'she'},
      {id:5, name:'it'},
      {id:6, name:'we'},
      {id:7, name:'they'},
    ];
    this.complements=[
      {id:1, name:'very hard'},
      {id:2, name:'every day'},
      {id:3, name:'in my class'},
      {id:4, name:'a book'},
      {id:5, name:'a letter'},
      {id:6, name:'sports whit my friend'},
      {id:7, name:'a time machine'},
      {id:8, name:'an incredible pet'},
      {id:9, name:'a song'},
      {id:10, name:'in the park'},
    ];
    
    this.verbs=[
        {id:1, name:'make' ,status:1, p_simple:'made', p_participle:'made'},
        {id:2, name:'play' ,status:0},
        {id:3, name:'read' ,status:1, p_simple:'read', p_participle:'read'},
        {id:4, name:'send' ,status:1, p_simple:'sent', p_participle:'sent'},
        {id:5, name:'show' ,status:1, p_simple:'showed', p_participle:'shown'},
        {id:6, name:'wish' ,status:0},
        {id:7, name:'come' ,status:1, p_simple:'came', p_participle:'come'},
        {id:8, name:'work' ,status:0},
        {id:9, name:'sing' ,status:1, p_simple:'sang', p_participle:'sung'},
        {id:10, name:'sleep' ,status:1, p_simple:'slept', p_participle:'slept'},
        {id:11, name:'eat' ,status:1, p_simple:'ate', p_participle:'eaten'},
        {id:12, name:'need' ,status:0},
        {id:13, name:'start' ,status:0},
        {id:14, name:'watch' ,status:0},
        {id:15, name:'walk' ,status:0}
    ];

    if(this.tSent=='1'){
      this.tiSent='Presente';
      this.stiSent='Simple';
      
    }else if(this.tSent=='2'){
      this.tiSent='Pasado';
      this.stiSent='Simple';
      
    }else if(this.tSent=='3'){
      this.tiSent='Futuro';
      this.stiSent='Simple';
      
    }else if(this.tSent=='4'){
      this.tiSent='Presente';
      this.stiSent='Continuo';

    }else if(this.tSent=='5'){
      this.tiSent='Pasado';
      this.stiSent='Continuo';
    }else if(this.tSent=='6'){
      this.tiSent='Presente';
      this.stiSent='Perfecto';
    }else if(this.tSent=='7'){
      this.tiSent='Pasado';
      this.stiSent='Perfecto';
    }

  }

  setVerb(sVerb){
    this.s_verb=sVerb;
  }

  setSentence(sComp){
    var s_aux;
    var randPron;
    var s_Comp;
    var s_verb;
    var s_name;

    s_verb=this.s_verb;

    randPron = this.pronouns[Math.floor(Math.random() * this.pronouns.length)];
    
    s_Comp=sComp.name;

    if(this.tSent=='1'){
        
        if((randPron.name=='He') || (randPron.name=='She') || (randPron.name=='It')){
          s_aux='does';
          s_name=s_verb.name+'s';
        }else {
          s_name=s_verb.name;
          s_aux='do';
        }


        this.sAfirm=randPron.name+' '+s_name+' '+s_Comp;
        this.sNegat=randPron.name+' '+s_aux+'n`t '+s_verb.name+' '+s_Comp;
        this.sInte=s_aux+' '+randPron.name+' '+s_verb.name+' '+s_Comp+'?';
    }else if(this.tSent=='2'){
        if(s_verb.status=='0'){
          s_name=s_verb.name;
          if(s_name.substr(-1,1)=='e'){
            s_name=s_verb.slice(0, -1);          
          }
          s_name=s_name+'ed';
        }else{
          s_name=s_verb.p_simple;
        }

        this.sAfirm=randPron.name+' '+s_name+' '+s_Comp;
        this.sNegat=randPron.name+' '+'didn`t'+' '+s_verb.name+' '+s_Comp;
        this.sInte='did'+' '+randPron.name+' '+s_verb.name+' '+s_Comp+'?';
    }else if(this.tSent=='3'){
        this.sAfirm=randPron.name+' will '+s_verb+' '+s_Comp;
        this.sNegat=randPron.name+' won`t'+' '+s_verb+' '+s_Comp;
        this.sInte='will'+' '+randPron.name+' '+s_verb+' '+s_Comp+'?';
    }else if(this.tSent=='4'){
      if((randPron.name=='He') || (randPron.name=='She') || (randPron.name=='It')){
        s_aux='is';
      }else if(randPron.name=='I'){
        s_aux='am';
      }else{
        s_aux='are';
      }

      s_name=s_verb.name;
      if(s_name.substr(-1,1)=='e'){
        s_name=s_name.slice(0, -1);
      }else if(s_name.substr(-2,2)=='ie'){
        s_name=s_name.slice(0, -2);
        s_name=s_name+'y';

      }else if((s_name.substr(-1,1)!='a')&&(s_name.substr(-1,1)!='e')&&(s_name.substr(-1,1)!='i')&&(s_name.substr(-1,1)!='o')&&(s_name.substr(-1,1)!='u')){
        if((s_name.substr(-2,1)=='i')||(s_name.substr(-2,1)=='o')||(s_name.substr(-2,1)=='u')){
          s_name=s_name+s_name.substr(-1,1);
        }
      }else{
        s_name=s_name;
      }

      this.sAfirm=randPron.name+' '+s_aux+' '+s_name+'ing'+' '+s_Comp;
      this.sNegat=randPron.name+' '+s_aux+' not '+s_name+'ing'+' '+s_Comp;
      this.sInte=s_aux+' '+randPron.name+' '+s_name+'ing'+' '+s_Comp+'?';
    
    }else if(this.tSent=='5'){
      if((randPron.name=='He') || (randPron.name=='She') || (randPron.name=='It' || randPron.name=='I')){
        s_aux='Was';
      }else{
        s_aux='Were';
      }

      s_name=s_verb.name;
      if(s_name.substr(-1,1)=='e'){
        s_name=s_name.slice(0, -1);
      }else if(s_name.substr(-2,2)=='ie'){
        s_name=s_name.slice(0, -2);
        s_name=s_name+'y';

      }else if((s_name.substr(-1,1)!='a')&&(s_name.substr(-1,1)!='e')&&(s_name.substr(-1,1)!='i')&&(s_name.substr(-1,1)!='o')&&(s_name.substr(-1,1)!='u')){
        if((s_name.substr(-2,1)=='i')||(s_name.substr(-2,1)=='o')||(s_name.substr(-2,1)=='u')){
          s_name=s_name+s_name.substr(-1,1);
        }
      }else{
        s_name=s_name;
      }


        this.sAfirm=randPron.name+' '+s_aux+' '+s_name+'ing'+' '+s_Comp;
        this.sNegat=randPron.name+' '+s_aux+' not '+s_name+'ing'+' '+s_Comp;
        this.sInte=s_aux+' '+randPron.name+' '+s_name+'ing'+' '+s_Comp+'?';
      
    }else if(this.tSent=='6'){
      if((randPron.name=='He') || (randPron.name=='She') || (randPron.name=='It')){
        s_aux='has';
      }else{
        s_aux='have';
      }

      if(s_verb.status=='0'){
        s_name=s_verb.name;
        if(s_name.substr(-1,1)=='e'){
          s_name=s_verb.slice(0, -1);
          s_name=s_name+'ed';          
        }
      }else{
        s_name=s_verb.p_participle;
      }

        this.sAfirm=randPron.name+' '+s_aux+' '+s_name+' '+s_Comp;
        this.sNegat=randPron.name+' '+s_aux+' not '+s_name+' '+s_Comp;
        this.sInte=s_aux+' '+randPron.name+' '+s_name+' '+s_Comp+'?';
    }else if(this.tSent=='7'){
      if(s_verb.status=='0'){
        s_name=s_verb.name;
        if(s_name.substr(-1,1)=='e'){
          s_name=s_verb.slice(0, -1);
          s_name=s_name+'ed';          
        }
      }else{
        s_name=s_verb.p_participle;
      }

      this.sAfirm=randPron.name+' '+'had'+' '+s_name+' '+s_Comp;
      this.sNegat=randPron.name+' '+'had'+' not '+s_name+' '+s_Comp;
      this.sInte='Had'+' '+randPron.name+' '+s_name+' '+s_Comp+'?';
    }
  }

  goBack(){
    this.navCtrl.setRoot(HomePage);
  }

}

